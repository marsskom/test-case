<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m181105_134222_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'user' => $this->integer(11),
            'done' => $this->tinyInteger(1),
            'created' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
