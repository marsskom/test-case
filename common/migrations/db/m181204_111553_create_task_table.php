<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m181204_111553_create_task_table extends Migration
{
    private $table = '{{%task_new}}';

    public function tableName($table)
    {
        $table = str_replace(['{{', '}}'], '', $table);
        $table = str_replace('%', Yii::$app->db->tablePrefix, $table);
        return $table;
    }

    public function init()
    {
        $this->table = $this->tableName($this->table);

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'text' => $this->string(),
            'checked' => 'TINYINT(1) NOT NULL DEFAULT 0',
            'order' => $this->integer()->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->unsigned()->defaultValue(null),
        ]);

//        $this->addPrimaryKey(
//            'pk-' . $this->table . '-id',
//            $this->table,
//            'id'
//        );

        $this->createIndex(
            'idx-' . $this->table . '-checked-order',
            $this->table,
            ['checked', 'order']
        );

        $this->addForeignKey(
            'fk-' . $this->table . '-user_id',
            $this->table,
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-' . $this->table . '-user_id', $this->table);
        $this->dropIndex('idx-' . $this->table . '-checked-order', $this->table);
//        $this->dropPrimaryKey('pk-' . $this->table . '-id', $this->table);
        $this->dropTable($this->table);
    }
}
