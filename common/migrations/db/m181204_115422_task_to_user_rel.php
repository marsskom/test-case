<?php

use yii\db\Migration;

/**
 * Class m181204_115422_task_to_user_rel
 */
class m181204_115422_task_to_user_rel extends Migration
{
    private $task = '{{%task_new}}';
    private $user = '{{%user}}';
    private $table = '{{%task_to_user}}';

    public function tableName($table)
    {
        $table = str_replace(['{{', '}}'], '', $table);
        $table = str_replace('%', Yii::$app->db->tablePrefix, $table);
        return $table;
    }

    public function init()
    {
        $this->table = $this->tableName($this->table);
        $this->task = $this->tableName($this->task);
        $this->user = $this->tableName($this->user);

        parent::init();
    }

    public function up()
    {
        $this->createTable($this->table, [
            'task_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(
            'pk-' . $this->table . '-task_id-user_id',
            $this->table,
            ['task_id', 'user_id']
        );

        $this->addForeignKey(
            'fk-' . $this->table . '-task_id',
            $this->table,
            'task_id',
            $this->task,
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-' . $this->table . '-user_id',
            $this->table,
            'user_id',
            $this->user,
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-' . $this->table . '-task_id', $this->table);
        $this->dropForeignKey('fk-' . $this->table . '-user_id', $this->table);
        $this->dropTable($this->table);
    }
}
