<?php

namespace common\models;

use yii\base\Model;

/**
 * Class Person
 * @package common\models
 */
class Person extends Model
{
    const STATUS_ACTIVE = true;

    public $name;
    public $age;
    public $verified;
    public $status;

    public function rules()
    {
        return [
            [['name', 'age'], 'required'],
            ['name', 'string', 'min' => 4, 'max' => 255],
            ['age', 'integer'],
            [['verified', 'status'], 'boolean'],
            ['verified', 'default', 'value' => 0],
            ['name', 'filter', 'filter' => function ($value) {
                return trim($value, '"');
            }],
            ['status', 'in', 'range' => array_keys(self::statuses())],
        ];
    }

    public static function statuses()
    {
        return [
            !self::STATUS_ACTIVE => 'Not active',
            self::STATUS_ACTIVE => 'Active',
        ];
    }
}
