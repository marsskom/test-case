<?php

namespace common\models;

use common\models\query\TaskQuery;
use frontend\behaviors\OrderBehavior;
use frontend\behaviors\TaskBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $done
 * @property string $created
 *
 * @property User $user
 */
class Task extends ActiveRecord
{
    const STATUS_DONE = true;

    public function behaviors()
    {
        return [
            'task.behavior' => [
                'class' => TaskBehavior::class,
                'attr' => 'done',
                'value' => self::STATUS_DONE,
            ],
            OrderBehavior::class,
        ];
    }

    public function transactions()
    {
        return [
            'default' => self::OP_ALL,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @return TaskQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'user_id', 'done', 'created'], 'required', 'message' => "Required {attribute}!"],
            [['name'], 'string'],
            [['user_id'], 'integer'],
            [['created'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['done'], 'string', 'max' => 1],
            ['order', 'integer'],
            ['order', 'default', 'value' => 1],
            ['user_id', 'exist', 'skipOnEmpty' => true, 'targetClass' => User::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_id' => 'User',
            'done' => 'Done',
            'created' => 'Created',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, [User::tableName() . '.id' => self::tableName() . '.user_id']);
    }

    public function getTaskToUser()
    {
        return $this->hasMany(TaskToUser::class, ['task_id' => 'id'])->andOnCondition([TaskToUser::tableName() . '.active' => 1]);
    }

    public function getUsers()
    {
//        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('{{%task_to_user}}', ['task_id' => 'id']);

        return $this->hasMany(User::class, ['id' => 'user_id'])->via('taskToUser');
    }
}
