<?php

namespace common\models;

class TaskSearch
{
    public function simpleSearch($keyword)
    {
        return Task::find()->andFilterWhere(['like', 'name', $keyword])->all();
    }
}