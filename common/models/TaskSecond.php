<?php

namespace common\models;

use common\models\enum\TaskEvent;
use frontend\behaviors\TaskBehavior;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property int $user
 * @property int $done
 * @property string $created
 */
class TaskSecond extends ActiveRecord
{
    const STATUS_DONE = true;

    public function behaviors()
    {
        return [
            'task.behavior' => [
                'class' => TaskBehavior::class,
                'value' => function () {
                    return true;
                },
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    TaskEvent::EVENT_TASK_DONE => 'done',
                ],
                'value' => function ($event) {
                    /** @var $event TaskEvent */
                    return self::STATUS_DONE;
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'user', 'done', 'created'], 'required'],
            [['name'], 'string'],
            [['user'], 'integer'],
            [['created'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['done'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user' => 'User',
            'done' => 'Done',
            'created' => 'Created',
        ];
    }

    public static function getUserTasks($userid)
    {
        $userid = intval($userid);
        return Task::find()->andWhere(['user' => $userid])->all();
    }


}
