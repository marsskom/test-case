<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class TaskToUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%task_to_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'task_id'], 'required'],
            [['user_id', 'task_id'], 'integer'],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['task_id', 'exist', 'targetClass' => Task::class, 'targetAttribute' => 'id'],
        ];
    }

//    public function getUser()
//    {
//        return $this->hasOne(User::class, ['id' => 'user_id']);
//    }
//
//    public function getTask()
//    {
//        return $this->hasOne(Task::class, ['id' => 'task_id']);
//    }
}
