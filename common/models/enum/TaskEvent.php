<?php

namespace common\models\enum;

use yii\base\Event;

/**
 * Class TaskEvent
 * @package common\models\enum
 */
class TaskEvent extends Event
{
    const EVENT_TASK_DONE = 'model.task.setDone';

    /**
     * @var array
     */
    public $attributes;
}
