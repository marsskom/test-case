<?php

namespace common\models\forms;

use yii\base\Model;
use common\models\TaskSearch;

class SearchForm extends Model
{
    public $keyword;

    public function rules()
    {
        return [
            [['name'], 'safe'],
        ];
    }

    public function search()
    {
        if ($this->validate()){
            $model = new TaskSearch();
            return $model->simpleSearch($this->keyword);
        }
    }

}