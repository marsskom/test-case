<?php

namespace common\models\query;

use common\models\Task;
use yii\db\ActiveQuery;

/**
 * Class TaskQuery
 * @package common\models\query
 */
class TaskQuery extends ActiveQuery
{
    /**
     * @return TaskQuery
     */
    public function active()
    {
        return $this->andWhere([Task::tableName() . '.active' => true]);
    }

    /**
     * @return TaskQuery
     */
    public function defaultOrder()
    {
        return $this->orderBy([Task::tableName() . '.order' => SORT_ASC]);
    }
}
