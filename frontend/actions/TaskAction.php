<?php
/**
 * Created by PhpStorm.
 * User: sergiy
 * Date: 2018-11-26
 * Time: 14:28
 */

namespace frontend\actions;

use common\models\enum\TaskEvent;
use frontend\controllers\AjaxController;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * Class TaskAction
 * @package frontend\actions
 */
class TaskAction extends Action
{
    /**
     * @var AjaxController
     */
    public $controller;

    /**
     * @param $id
     */
    public function run($id)
    {
        try {
            /** @var $model ActiveRecord */
            $model = $this->controller->findModel($id);
        } catch (NotFoundHttpException $e) {
            $this->controller->data = ['error' => $e->getMessage()];
            return;
        }

        $model->trigger(TaskEvent::EVENT_TASK_DONE, new TaskEvent([
            'attributes' => $model->attributes,
        ]));

        $this->controller->data = ['done' => $model->done];
    }
}
