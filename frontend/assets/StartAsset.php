<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class StartAsset extends AssetBundle
{
    public $css = [
        '/css/start/style.css',
    ];
}