<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class TaskAsset extends AssetBundle
{
    public $css = [
        '/css/task/style.css',
    ];
}