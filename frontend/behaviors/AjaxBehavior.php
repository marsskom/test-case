<?php

namespace frontend\behaviors;

use yii\base\ActionFilter;
use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use common\models\Task;

class AjaxBehavior extends ActionFilter
{
    public function events()
    {
        return[
            Controller::EVENT_BEFORE_ACTION => 'ajaxQuery',
        ];
    }

    public function ajaxQuery()
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Is not ajax');
        }

        return true;
    }
}
