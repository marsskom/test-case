<?php
/**
 * Created by PhpStorm.
 * User: sergiy
 * Date: 2018-11-26
 * Time: 13:46
 */

namespace frontend\behaviors;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class OrderBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;

    public $attr = 'order';
    public $value;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'setOrder',
        ];
    }

    public function setOrder($event)
    {
        $value = is_null($this->value) ? $this->owner->id : $this->value;
        $this->owner->{$this->attr} = $value;
        $this->owner->updateAttributes([$this->attr]);
    }
}
