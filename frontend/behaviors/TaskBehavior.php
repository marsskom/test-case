<?php

namespace frontend\behaviors;

use common\models\enum\TaskEvent;
use common\models\Task;
use yii\base\Behavior;
use yii\base\InvalidConfigException;

/**
 * Class TaskBehavior
 * @package frontend\behaviors
 */
class TaskBehavior extends Behavior
{
    /**
     * @var Task
     */
    public $owner;

    /**
     * @var string
     */
    public $attr = 'done';
    /**
     * @var mixed
     */
    public $value;

    public function init()
    {
        if (is_null($this->value)) {
            throw new InvalidConfigException('Value must be set');
        }

        parent::init();
    }

    public function events()
    {
        return [
            TaskEvent::EVENT_TASK_DONE => 'setDone',
        ];
    }

    /**
     * @param TaskEvent $event
     */
    public function setDone($event)
    {
        $value = is_callable($this->value) ? call_user_func($this->value) : $this->value;

        $this->owner->{$this->attr} = $value;
        $this->owner->updateAttributes([$this->attr]);
    }
}
