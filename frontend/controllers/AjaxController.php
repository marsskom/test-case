<?php

namespace frontend\controllers;

use common\models\enum\TaskEvent;
use common\models\TaskSecond;
use frontend\actions\TaskAction;
use frontend\behaviors\AjaxBehavior;
use yii\filters\AjaxFilter;
use yii\web\Controller;
use Yii;
use common\models\Task;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers
 */
class AjaxController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @var Response
     */
    protected $response;
    public $data;

    public function behaviors()
    {
        return [
            AjaxFilter::class,
        ];
    }

    public function actions()
    {
        return [
            'mark' => [
                'class' => TaskAction::class,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->response = Yii::$app->response;

        parent::init();
    }

    public function beforeAction($action)
    {
        $res = parent::beforeAction($action);

        if ($res) {
            $this->response = Response::FORMAT_JSON;
        }

        return $res;
    }

    public function afterAction($action, $result)
    {
//        parent::afterAction($action, $result);

        $this->response->data = $this->data;
        $this->response->send();
        Yii::$app->end();
    }

    public function actionMark($id)
    {
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
            $this->data = ['error' => $e->getMessage()];
            return;
        }

        $model->trigger(TaskEvent::EVENT_TASK_DONE, new TaskEvent([
            'attributes' => $model->attributes,
        ]));

        $this->data = ['done' => $model->done];
    }

    public function actionCsrf()
    {
        $token = Yii::$app->request->post('csrf');
    }

    public function findModel($id)
    {
        if (($model = TaskSecond::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
