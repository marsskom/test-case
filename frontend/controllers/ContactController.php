<?php

namespace frontend\controllers;

use frontend\forms\ContactForm;
use frontend\models\Contact;
use yii\web\Controller;
use Yii;

/**
 * Class ContactController
 * @package frontend\controllers
 */
class ContactController extends Controller
{
    public function actionIndex()
    {
        //$model = new ContactForm();
        $model = new Contact();

        if (Yii::$app->request->isPost) {
            /**
             * [
             *  'ContactForm' => [
             *      'name' => '...',
             *      'email' => '...',
             *      'message' => '...',
             *  ]
             * ]
             */
            if ($model->load($_POST) && $model->validate()) {
                $model->save();

//                $model = new ContactForm();
                $model = new Contact();
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }
}
