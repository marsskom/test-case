<?php
/**
 * Created by PhpStorm.
 * User: sergiy
 * Date: 2018-11-28
 * Time: 13:22
 */

namespace frontend\forms;

use frontend\models\Contact;
use yii\base\Model;

/**
 * Class ContactForm
 * @package frontend\forms
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $message;

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],
            ['name', 'string', 'max' => 255],
            ['message', 'string', 'max' => 5000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => "Ім'я",
            'email' => 'Email',
            'message' => 'Message',
        ];
    }

    public function save()
    {
        $contact = new Contact($this->attributes);
        return $contact->save();
    }
}
