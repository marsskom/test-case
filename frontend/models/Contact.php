<?php
/**
 * Created by PhpStorm.
 * User: sergiy
 * Date: 2018-11-28
 * Time: 13:22
 */

namespace frontend\models;

use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * Class Contact
 * @property $id integer
 * @property $name string
 * @property $email string
 * @property $message string
 * @property $user_id integer
 *
 * @package frontend\models
 */
class Contact extends ActiveRecord
{
    public $attachments;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%contact}}';
    }

    public function init()
    {
        parent::init();

        if (!Yii::$app->user->isGuest) {
            $this->user_id = Yii::$app->user->getId();
            $this->name = Yii::$app->user->identity->username;
            $this->email = Yii::$app->user->identity->email;
        }
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            // Behavior works with attachments
        ];
    }

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['email', 'email'],
            ['name', 'string', 'max' => 255],
            ['message', 'string', 'max' => 5000],
            ['user_id', 'exist', 'skipOnEmpty' => true, 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['attachments', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => "Ім'я",
            'email' => 'Email',
            'message' => 'Message',
        ];
    }
}
