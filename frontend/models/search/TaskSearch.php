<?php
/**
 * Created by PhpStorm.
 * User: sergiy
 * Date: 2018-11-28
 * Time: 14:39
 */

namespace frontend\models\search;

use common\models\Task;

/**
 * Class TaskSearch
 * @package frontend\models\search
 */
class TaskSearch extends Task
{
    /**
     * @param $userId
     * @return array|Task[]|\yii\db\ActiveRecord[]
     */
    public static function getUserTasks($userId)
    {
        $userId = intval($userId);
        return Task::find()->andWhere(['user' => $userId])->all();
    }
}
