<?php

/** @var $this \yii\web\View */
/** @var $model \frontend\forms\ContactForm */
?>

<?php $form = \yii\widgets\ActiveForm::begin([
    'method' => 'post'
]); ?>

<?= $form->errorSummary($model) ?>

<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'email')->textInput() ?>
<?= $form->field($model, 'message')->textarea() ?>

<?= \yii\helpers\Html::submitButton('Send') ?>
<?php \yii\widgets\ActiveForm::end(); ?>
