<?php
/* @var $model common\models\forms\SearchForm  */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>


<div class="">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'keyword') ?>

    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

    <?php ActiveForm::end(); ?>
</div>
<div class="">
    <?php if ($results): ?>
    <?php foreach ($results as $item): ?>
    <?= $item['name']?>
    <hr>
    <?php endforeach; ?>
    <?php endif; ?>


</div>

