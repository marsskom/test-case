<?php

use yii\helpers\Url;
use frontend\assets\TaskAsset;
use yii\base\BaseObject;
use yii\base\Component;

TaskAsset::register($this);
?>
<div class="contain">
    <h1>Список завдань</h1>
    <?php if (!empty($list)) : ?>
        <ul class="items">
            <?php foreach ($list as $item): ?>
                <li>
                    <span class="item <?= $item['done'] ? 'done' : '' ?>"><?= $item['name'] ?></span>
                    <?php if (!$item['done']): ?>
                        <a href="<?= Url::to(['ajax/mark', 'as' => 'done', 'id' => $item['id']]) ?>" class="btn btn-success">Виконано</a>
                    <?php endif; ?>
                    <a href="<?= Url::to(['update', 'id' => $item['id']]) ?>" class="btn btn-warning">Редагувати</a>
                    <a href="<?= Url::to(['delete', 'id' => $item['id']]) ?>" data-method="post" class="btn btn-danger">Видалити</a>
                </li>
            <?php endforeach; ?>=
        </ul>
    <?php else: ?>
        <p>Задач немає</p>
    <?php endif; ?>
    <br>
    <a href="<?= Url::to(['create']) ?>" class="btn btn-primary">Додати завдання</a>
</div>


